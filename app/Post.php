<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    public $directory = "/images/"; 

    protected $fillable = [
        // 'id',
        'title',
        'body',
        'path'
    ];

    public function User()
    {
        // return $this->belongsTo(User::class);
        return $this->belongsTo('App\User');
    }

    public function getPathAttribute($value){
        $directory = "/images/"; 
        
        return $this->$directory . $value;
    }
    
}
