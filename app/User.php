<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function post(){
        // return $this->hasMany(Post::class);
        return $this->hasMany('App\Post');
    }


    public function getNameAttribute($value){
        return ucfirst($value);
    }

    public function serNameAttribute($value){
        $this->attributes['name'] = strtoupper($value);
    }

    public static function scopeLatest($query){
        return $query->orderBy('id','asc')->get();
    }

    // Relacion con tabla ROLE
    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function isAdmin(){
        // echo "<br>";
        // echo "Ingreso a isAdmin";
        // echo "<br>";
        // echo "<br>";

        // echo "this->role->name : " . $this->role_id;

        // echo "<br>";
        // echo "<br>";
        
        // return  "Ingreso a isAdmin, adios!";
        if($this->role_id == 1){
            echo "<br> Eres Admin !! <br>";
            return true;
        }
        echo "<br>  No eres Admin :( <br> ";
        return false;
    }
}
