<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\User;
use App\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
    // $user = Auth::user();
    
    // echo "user is : " . $user->isAdmin();

    // if($user->isAdmin()){
    //     echo "congratulations ! You are admin!";
    // }else{
    //     echo "You no Admin :(";
    // }

});
/*

Route::get('/create', function() {
    // $user = User::findOrFail(1);

    $retorno = App\User::find(1)->name;

    echo $retorno;
    // return ;

    // $user->posts()->save(new Post(['title'=>'Mi primer titulo de posteo','body'=>'Cuerpo en post primero']));
});

Route::get('/relacion', function () {

    $retorno = App\Post::find(1)->user;

    echo "La relacion entre post y user es : " . $retorno;
});


Route::get('/read', function () {

    $user = User::findOrFail(1);

    // return dd($user);   // Retorna en forma de objeto
    return $user->posts;
});

Route::get('/read1', function () {

    $post = Post::findOrFail(1);

    return $post->user;
});

// =========================
//  
// =========================

Route::group(['middleware'=>'web'],function(){
    Route::resource('/posts', 'PostsController');

    
    Route::get('/dates', function() {
        $date = new DateTime('+1 week');

        echo $date->format('m-d-Y');

        echo '<br>';

        echo Carbon::now()->addDays(10)->diffForHumans();

        echo '<br>';
        echo '<br>';

        echo Carbon::now()->subMonths(5)->diffForHumans();
        
        echo '<br>';
        echo '<br>';

        echo Carbon::now()->yesterday()->diffForHumans();
    });
    
    Route::get('/getname', function() {
        $user = User::find(1);

        echo $user;
    });

    Route::get('/setname', function() {
        $user = User::find(1);

        $user->name = "William";

        $user->save();
    });

});
*/
Route::auth();

Route::get('/home', 'HomeController@index');

// =========================
//  Para aplicar middleware - Seccion 23
// =========================


Route::get('/admin/user/roles',['middleware'=>['role','auth','web'] , function() {

    return "Middleware role";

}]);


Route::get('/admin', 'AdminController@index' );
