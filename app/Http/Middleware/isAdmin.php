<?php

namespace App\Http\Middleware;

use Closure;
use Illu\Http\Middleware;
use Illuminate\Support\Facades\Auth;


class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // echo "<br>";
        // echo "Ingreso en handle isAdmin.php";
        // echo "<br>";
        $user = Auth::user();

        // echo "<br>";
        // echo "User$ es " . $user;
        // echo "<br>";

        echo "<br>";
        // echo "User$->isadmin es " . $user->isAdmin();
        echo "<br>";

        // Si no eres admin, se redirecciona a login ; Recordar que el usuario debe tener una sesion iniciada
        if(!$user->isAdmin()){
            return redirect()->intended('/login');
        }

        return $next($request);
    }
}
