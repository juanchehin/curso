<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::latest()->get();

        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return "Ingresaste a create!!";
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreatePostRequest $request)
    {
        // Persistiendo datos en la BD

        $input = $request->all();

        if($file = $request->file('file')){

            // Obtengo el nombre original de el archivo subido por el cliente
            $name = $file->getClientOriginalName();

            // Muevo la imagen a la carpeta especificada
            $file->move('images',$name);

            $input['path'] = $name;
        }

        Post::create($input);




        // Subida de archivos
        // $file = $request->file('file');

        // echo "<br>";

        // echo $file->getClientOriginalName();

        // echo "<br>";
        // echo "<br>";
        // echo "Tamaño del archivo en bytes : " . $file->getClientSize();

        // Validacion de datos

        // $this->validate($request,[
        //     'title'=>'required|max:4',
        //     // 'content'=>'required'
        //     'body'=>'required'
        // ]);

        
        // print_r("Ingreso en stores");
        // return "Ingreso en stores";

        // return $request->all();

        // Inserto los datos en la BD
        // Post::create($request->all());

        // return redirect('/posts');

        // $input = $request->all();

        // $input['title'] = $request->title;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::findOrFail($id);

        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        echo "id es : " . $id . "<br>";

        $post = Post::findOrFail($id);

        // echo " post es : " . $post;

        // print_r($post);
        return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post = Post::findOrFail($id);

        $post->update($request->all());

        return redirect('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $post = Post::findOrFail($id);

        $post = Post::whereId($id)->delete();

        // $post->delete();

        return redirect('/posts');
    }
}
