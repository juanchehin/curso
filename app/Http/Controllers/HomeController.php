<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Llamo al middleare
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // ------------
        // Sesiones - SECCION 24
        // ------------

        // $request->session()->put(['edwin'=>'master instructor']);    // Almaceno los datos en la sesion
        // session(['peter'=>'student']);  // Almaceno los datos en la sesion (otra manera)
        // session(['edwin2'=>'Tu profesor']);
        // return session('edwin2');

        // $request->session()->forget('edwin3');  // Elimina un dato
        // $request->session()->flush('edwin3');  // Elimina todos los datos de sesion
        // echo "<br><br> request->session()->get('edwin') : "  . $request->session()->get('master instructor') . "<br><br>";
        // return $request->session()->all();  // Devuelvo todas las variables que almacene

        // $request->session()->flash('message','Post has been created');  // flash : solo vive hasta la proxima solicitud

        $request->session()->reflash('message','Post has been created 2');  // Almaceno los datos para sesiones adicionales

        $request->session()->keep('message','Post has been created 2'); // Especifico que dato deseo almacenar

        return $request->session()->get('message');

        // ------------
        // FIN Sesiones
        // ------------
        // return view('home');
    }
}
