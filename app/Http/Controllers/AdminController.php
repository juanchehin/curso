<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    //

    public function __construct()
    {
        echo "ingreso en __contructor en AdminController";
        echo "<br>";
        echo "<br>";
        
        $this->middleware('isAdmin');    // E aqui el problema, ja!
    }
    public function index()
    {
        return "You are an administrator because you seeing this page";
    }
}
