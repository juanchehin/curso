@extends('layouts.app')


@section('content')

    <h1>Create Post</h1>

    {{-- <form method="POST" action="/posts"> --}}
 
    {!! Form::open(['method'=>'POST','action'=>'PostsController@store', 'files'=>true]) !!}

    
        <div class="form-group">

            {!! Form::label('title','Title:') !!}

            {!! Form::text('title',null,['class'=>'form-control']) !!}

        </div>

        <div class="form-group">

            {!! Form::label('File','Archivo : ') !!}

            {!! Form::file('file',null,['class'=>'form-control']) !!}

        </div>

        {{-- <input type="text" name="title" placeholder="Ingresa un titulo..."> --}}

        {!! Form::submit('Create Post',['class'=>'btn btin-primary']) !!}

    <div class="form-group">

        <input type="submit" name="submit">

    </div>
    {!! Form::close() !!}

    @if(count($errors) > 0)

        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>

    @endif

@endsection

    {{-- </form> --}}

{{-- @yield('footer') --}}