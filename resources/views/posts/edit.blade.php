@extends('layouts.app')


@section('content')

    <h1>Edit Post</h1>

    {{-- <form method="POST" action="/posts/{{$post->id}}"> --}}

        {!! Form::model([$post, ['method'=>'PATCH','action'=>'PostsController@update',$post->id]]) !!}

            {{csrf_field()}}

        {!! Form::label('title','Title:') !!}

        {!! Form::text('title',null,['class'=>'form-control']) !!}

        {!! Form::submit('Update Post', ['class'=>"btn btn-info"]) !!}

        {!! Form::close() !!}

        {!! Form::open(['method'=>'DELETE','action'=>['PostsController@destroy',$post->id]]) !!}

        {!! Form::submit('Delete Post', ['class'=>"btn btn-danger"]) !!}

        {!! Form::close() !!}

{{--         
        <input type="hidden", name="_method" value="PUT">

        <input type="text" name="title" placeholder="Ingresa un titulo..." value="{{$post->title}}"> --}}

        {{-- <input type="submit" name="submit" value="UPDATE"> --}}

    {{-- </form>

    <form method="POST" action="/posts/{$post->$id}}">
    
        <input type="hidden" name="_method" value="DELETE">

        <input type="submit" value="DELETE">
    
    </form> --}}

@yield('footer')